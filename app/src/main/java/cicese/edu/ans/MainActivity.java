package cicese.edu.ans;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import cicese.edu.ans.camera.CameraSurfaceView;

/**
 * Created by alejandro on 6/15/14.
 * email: arangel@cicese.edu.mx
 */

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        final View contentView = findViewById(R.id.fullscreen_content);
        final CameraSurfaceView cameraView = (CameraSurfaceView)findViewById(R.id.camera_view);
    }

}
